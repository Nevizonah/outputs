Files extracted with QuickBMS v0.10.1

Using Scripts included in repo under eqbms directory

Example Script:

./quickbms /path/to/script/file/EQOA.bms /path/to/parse/file/EQOA.esf /path/to/output/location/

Current TODOs:

- get UV output
- get Textures
- convert 3ds -> obj -> glTF/glB (Binary form of glTF) 


-- multiple options here:
--- Use an extraction tool that reads textures from RAM while a game is loaded up in PCSx2 (potentially the easiest - but also buggy?)

--- revisit the QuickBMS scripts ( learn how to write/modify existing scripts to extract textures, this could be the fastest for getting all of the files done and to me is the most likely solution. We just need to traverse the learning curve here. Additionally this may retrieve UV maps for us).
--- Work with community already embedded in the game to figure out a way to query what i believe is the MySQL instance running in the emulator.

- convert 3ds -> obj -> GLtf/GLB(binary version of GLtf)
-- script to open, convert, and export to new file formats

Opening the files:

Note I needed to use a previous version of Blender that still had the ability to open (import) 3ds files.
I used https://www.blender.org/download/releases/2-79/

You will need to follow the steps to turn on the 3ds import add-on 
https://docs.blender.org/manual/en/2.79/preferences/addons.html

https://docs.blender.org/manual/en/2.79/addons/io_3ds.html




